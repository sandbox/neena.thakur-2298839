<?php 

function fb_page_feed_config_form($form, &$form_state){
	
  $form['#suffix'] = '<div class="notes">This feature is available as Block</div>';	
  
  $form['facebook']['fb_page_name'] = array(
	'#type' => 'textfield',
	'#default_value' => variable_get('fb_page_name', 'LungForce'),
	'#title' => t('Facebook Page Name'),
	'#size' => 40,
	'#maxlength' => 120,
	'#required' => TRUE,
	);	
	
  $form['facebook']['fb_no_of_post'] = array(
	'#type' => 'textfield',
	'#default_value' => variable_get('fb_no_of_post', '5'),
	'#title' => t('Number of Posts'),
	'#size' => 40,
	'#maxlength' => 120,
	'#required' => TRUE,
	);	
	
   
  return system_settings_form($form);
}

